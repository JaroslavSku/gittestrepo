import { passengerDto } from './dto/passenger.dto';
import { PassengerService } from './passenger.service';
import { Controller, Get, Post, Body, Param } from '@nestjs/common';

@Controller('api/v1/passenger')
export class PassengerController {
  constructor(private readonly passengerService: PassengerService) {}
  @Get()
  async createPassenger(@Body() dto: passengerDto) {
    return this.passengerService.create(dto);
  }

  @Get()
  async getAllPassengers() {
    return this.passengerService.findAll();
  }
  @Get(':id')
  async getById(@Param('id') id: string) {
    return this.passengerService.findById(id);
  }
}
